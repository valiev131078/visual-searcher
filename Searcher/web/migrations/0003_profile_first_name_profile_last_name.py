# Generated by Django 4.2 on 2023-05-26 12:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_profile'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='first_name',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='profile',
            name='last_name',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
