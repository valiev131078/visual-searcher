from django.contrib import admin
from .models import Search, Fruit, Review

admin.site.register(Search)
admin.site.register(Fruit)
admin.site.register(Review)
