from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import get_user_model, authenticate, login, logout

from .models import Fruit
from .forms import ImageForm, RegistrationForm, AuthForm, ReviewForm

import tensorflow as tf
import numpy as np
import cv2

User = get_user_model()

model = tf.keras.applications.MobileNetV2(weights='imagenet')


def load_and_preprocess_image(image_path):
    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)  # Перевод изображения в RGB
    image = cv2.resize(image, (224, 224))  # Изменение размера изображения для модели
    image = np.expand_dims(image, axis=0)  # Добавление дополнительной размерности
    image = tf.keras.applications.mobilenet_v2.preprocess_input(image)  # Предобработка изображения для модели
    return image


def detect_logo(image_path):
    image = load_and_preprocess_image(image_path)
    predictions = model.predict(image)
    predicted_class = tf.keras.applications.mobilenet_v2.decode_predictions(predictions, top=1)[0][0]
    logo_name = predicted_class[1]
    return logo_name


def main_view(request):
    return render(request, "web/main.html")


def registration_view(request):
    form = RegistrationForm()
    is_success = False
    if request.method == 'POST':
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email']
            )
            user.set_password(form.cleaned_data['password'])
            user.save()
            print(form.cleaned_data)
            is_success = True
    return render(request, "web/registration.html", {"form": form, "is_success": is_success})


def auth_view(request):
    form = AuthForm()
    if request.method == 'POST':
        form = AuthForm(data=request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, "Введены неверные данные")
            else:
                login(request, user)
                return redirect("main")
    return render(request, "web/auth.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("main")


@login_required
def image_create(request):
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(user=request.user)
            image_path = form.instance.image.path
            logo_name = detect_logo(image_path)
            return render(request, 'web/image_create.html', {'form': form, 'logo_name': logo_name})
    else:
        form = ImageForm()
    return render(request, 'web/image_create.html', {'form': form})


@login_required
def profile(request):
    user = request.user
    if request.method == 'POST':
        user.first_name = request.POST.get('first_name')
        user.last_name = request.POST.get('last_name')
        user.save()
        return redirect('profile')

    return render(request, 'web/profile.html', {'user': user})


@login_required
def fruit_list(request):
    fruits = Fruit.objects.all()
    return render(request, 'web/fruit_list.html', {'fruits': fruits})


@login_required
def fruit_detail(request, fruit_id):
    fruit = get_object_or_404(Fruit, pk=fruit_id)
    reviews = fruit.reviews.all()

    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.user = request.user
            review.fruit = fruit
            review.save()
            return redirect('fruit_detail', fruit_id=fruit_id)
    else:
        form = ReviewForm()

    return render(request, 'web/fruit_detail.html', {'fruit': fruit, 'reviews': reviews, 'form': form})
